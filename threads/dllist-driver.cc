#include <iostream>
#include <cstdlib>
#include "dllist.h"
#include "thread.h"
#include "debug.h"
#include "string.h"
#include "main.h"
using namespace std;

DLList *list;
int T;
int N;

void RemoveNItems(int threadNum) {
	for (int i = 0; i < N; i++){
		int key;
		//list->Print();
		if (list->remove(&key) != NULL) {
			cout << "\t\tThread No." << threadNum << " has REMOVED item with key \'"
                << key << "\'"<< endl;
		}
        else {
            cout << "\t\tThread couldn't remove item. List was Empty." << endl;
        }
	}
}

void InsertNItems(int threadNum) {

	for(int i = 0; i < N; i++) {
		int key = rand() % 100;
		char* item = new char((rand() % 25) + 65);
	       list->SortedInsert(item, key);
		cout << "\t\t\t\tThread NO." << threadNum
            << " has INSERTED  item with key \'" << key << "\'" << endl;

	}
}

static void TestThreadTask(int threadNum) {
    InsertNItems(threadNum);
    //kernel->currentThread->Yield();
    RemoveNItems(threadNum);
    //kernel->currentThread->Yield();
}

void SelfTest() {

    TestThreadTask(0);
    for (int i = 1; i < T; i++) {
        Thread* t = new Thread("SelfTest");
        t->Fork((VoidFunctionPtr) TestThreadTask, (void*)i);
    }

}

void HW1Test(int argc, char** argv) {

    T = -1; // num of threads
    N = -1; // num of calling dllist driver function per each thread

    for (int i = 1; i < argc; i++) {
        if (strcmp(argv[i], "-h") == 0) {
            ASSERT(i + 2 < argc);
            T = atoi(argv[i + 1]);
            N = atoi(argv[i + 2]);
            i += 2;
        }
        else if(strcmp(argv[i], "-u") == 0) {
            cout << "Partial Usage: ./nachos -h #(T) #(N)" << endl;
        }
    }
    bool startTest = T > 0 && N > 0;
    DEBUG(startTest, "Starting HW1Test.");

    list = new DLList();
    SelfTest();
}
